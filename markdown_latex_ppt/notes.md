#### A voir:
* Intervalle de cohérence
* Random access preamble collision

# Random beamforming for mmWaves.


## Abstract:

Gros path loss en mmWave, donc repose sur la directionalité pour compenser.

Problème des beams:
* Complique la procédure d'accès initial.
* Augmente la latence puisque les beams de l'envoyeur et du receveur doivent être alignés.


## I. Introduction

Les mmWave offrent bcp plus de spectre que les fréquences congestionnées en-dessous de 6GHz.

### A. Related work and motivation

Presque toutes les méthodes proposées précédemment demandaient au transmetteur et au receveur de scanner tous les angles autour pour trouver le meilleur gain. Mais les data rates sont tellement hauts que ce n'est pas grave si un téléphone se connecte à une station de base qui n'est pas optimale: ça réduira la latence d'établissement du data plane.
--> C'est notamment le cas des wake-up radios.

### B. Contributions

* Utilisation de la géométrie stochastique pour évaluer la performance de l'accès initial avec un random beamforming.
* Evaluation détaillée du beamforming aléatoire pour l'accès initial (densité du réseau, obstacles...).
* Comparaison avec le scan exhaustif.


## II. System model

### A. Network model

On distribue les BSs avec une distribution Poisson 2D, ce qui modélise le shadowing, dont on ne tiendra donc plus compte dans la suite.
On distribue les UEs avec une autre loi de Poisson 2D indépendante de celle des BSs.

### B. Network model

Pour chaque UE, chaque BS est en ligne de vue avec une proba $p_{LOS}(r)$, avec r la distance.
On suppose que $p_{LOS}(r) = exp(-\beta r)$, avec $\beta$ un paramètre déterminé par la densité et la taille moyenne des obstacles. Rq: $\beta^{-1}$ est la longueur moyenne d'un lien en ligne de vue.
On donne aussi une formule pour le path loss en ligne de vue, et on suppose que ce qui n'est pas en ligne de vue est complètement bloqué.

_Single-path assumption_: Puisque la diffusion se fait mal avec les mmWaves, on pourra supposer qu'il n'existe qu'un seul chemin entre l'UE et chaque BS (i.e., pas de chemin alternatif).

On donne ensuite une formule pour la channel matrix, cf. page 6 du papier.


### C. Antenna model

#### 1. Sectorized Beam Pattern

L'empreinte 2D de l'antenne est constituée de 2 lobes: le principal et le secondaire, et presque tout est concentré sur le principal. Le lobe principal est divisé en un nombre $N_{BS}$ ou $N_{UE}$ de beamforming vectors.

#### 2. Uniform Linear Array

Quelques formules.
Plus le nombre de directional vectors est important, plus le gain sera grand.


### D. 3GPP NR Model

_SS_: Synchronization Signal.

_SS Block_: 4 OFDM symbols in time and 240 subcarrier in frequency.

_SS Burst_: Group of several SS blocks, dans {5, 10, 20, 40, 80, 160} ms. Plus la valeur est grande, moins l'overhead de synchronisation est important.


## III. Framework d'accès initial et mesures de performances

Le system time est divisé en deux phases entre chaque intervalle de cohérence:
1. Une période d'accès intial:
    * Recherche de cellule
    * Accès aléatoire

2. Période de transmission des données avec alignement des faisceaux


### A. Phase de recherche de la cellule

Cette phase prend plusieurs mini-slots.

A chaque mini-slot, l'UE choisit aléatoirement (uniformément) une direction de beamforming, et la BS parcourt toutes les directions.

_Scan cycle_: Période pendant laquelle tous les BS envoient des pilotes de recherche de cellule dans $N_{BS}$ directions. Pendant un cycle, l'UE pointe dans une direction aléatoire parmi les $N_{UE}$ possibles, et les BS couvrent toutes les $N_{BS}$ directions.

Quand l'UE reçoit un pilote supérieur au SINR minimal, il se connecte au BS correspondant. Toutefois, il pourra trouver mieux plus tard sans interruption de service.

### B. Phase d'accès aléatoire

Quand l'UE a trouvé une BS valable, il envoie des préambules à cette BS. Cette BS en déduit la direction de provenance -- **à vérifier**.

**_Supposition 1_**: Il n'y a pas de collisions de préambules, i.e. les BS détectent tous les préambules.

Jusification: La proba que deux UE choisissent le même préambule au même canal spatial est très petite, dû à la petite empreinte d'interférence des mmWaves -- **à revoir**.

**_Supposition 2_**: Le SINR threshold est le même pour les phases de recherche de la cellule et d'accès aléatoire.

Ces deux suppositions impliquent que l'accès aléatoire fonctionne _ssi_ la recherche de la cellule a fonctionné. En gros, la phase B fonctionne.


### C. Analyse du plan de données

**A revoir**: des maths avec l'_ULA model_.


### D. Analyse des performances

On nous donne une formule qui donne le SINR dans le modèle en fonction des paramètres.

_Probabilité d'échec de détection $P_f(N_c)$_: Proba que l'UE ne détecte aucune BS dans un intervalle de $Nc$ mini-slots.

_Latence d'accès initial $D_I(N_c)$_: Durée après laquelle l'UE détecte un cell-search pilot et peut être enregistré auprès de la BS correspondante.

_Latence de transmission de données $D_T(N_c)$_: Durée après laquelle l'UE s'enregistre avec succès à un BS et réussit la transmission du premier paquet.


## IV. Probabilité d'échec de la détection et analyse de la latence

Des maths, à revoir.

### A. Detection Failure Probability

### B. Delay analysis

## V. Numerical Results

## Références:

* [1] et [3] prouvent que mmWave est l'avenir des wireless devices futurs pour supporter des services de très haut débit.
* [5] montre les problèmes de propagation des mmWaves.
* [6] montre une solution préliminaire à base de grosses antennes pour mmWaves.
* [10] présente notre modèle de la sectorized antenna.




# mmWaves: a MAC perspective

## Abstract:

Les réseaux mmWave impliquent d'avoir plein d'antennes directionnelles, ce qui complique le MAC layer.

## Introduction

On va montrer de nouvelles approches pour 3 aspects:
1. Control channel architecture: On va montrer pourquoi avoir un canal de contrôle omnidirectionnel en plus du canal de contrôle directionnel est un plus.
2. Initial access and mobility management: On va montrer pourquoi les contention-based random access protocols sont mieux adaptés.


## Fondamentaux:

### A. The directed mmWave Wireless Channel

Avoir des longueurs d'onde courtes permet d'avoir de plus petites antennes, donc plein d'antennes dans un espace très petit: gain de directivité au prix d'un signal processing plus poussé.

_Blockage_: Gros penetration loss dû à un obstacle, et ne pouvent être résolu simplement en augmentant la puissance de transmission.
--> Pousse à redéfinir la notion traditionnelle de cellule.

_Deafness_: Situation où le beam du transmetteur et du receveur ne sont pas alignés. Avantage quand même: diminue fortement les interférences, puisque le receveur n'écoute qu'un espace restreint.

### B. Heterogeneity

#### Spectral heterogeneity:

Afin de dépasser les contraintes des mmWaves, le mécanisme MAC pourrait utiliser des microWaves (LTE) aussi.
--> Le control layer utiliserait des fréquences <= 6GHz tandis que les données utiliseraient des mmWaves pour avoir du gros bandwidth.

#### Deployment heterogeneity:

Il y aurait différents types de cellules (macro-, micro-, femto- et picocells).




## Références:

* [30] montre que la deafness permet de réduire les interférences.
* [15] montre l'utilité de s'intéresser aussi au micro-ondes.


# Initial Access in Millimeter Wave Cellular Systems

## I. Intro

En plus de se détecter mutuellement et de gérer l'access request, la BS et l'UE doivent trouver la BF direction qui conviendra.

_Control plane latency_: Durée que prend l'établissement de l'initial access (idle mode to connected state).

D'ailleurs, les procédures d'IA seront plus fréquentes en mmWaves, parce que:
1) Les stations seront très susceptibles au shadowing.
2) On utilisera des macrocells 4G pour couvrir le cas où il n'y a pas de service acceptable: beaucoup d'inter-RAT (_radio access technology_) handovers sont à prévoir.
3) On ira souvent en mode idle parce que l'utilisation de très hautes fréquences avec une grosse bande passante coûte beaucoup d'énergie.

Une différence entre les communications numériques et analogiques est que les communications numériques peuvent théoriquement regarder partout en même temps, alors que l'analogique ne permet qu'un angle restreint de beam.

### Contributions
* On va regarder plusieurs procédures d'IA en utilisant les principes de la 4G pour référence à la différence près que **l'UE et la BS doivent acquérir les directions BF initiales**.
* Un problème majeur est la grande consommation des antennes quand on fait tourner la BF direction. Du coup beaucoup de papiers font passer ça en analogique. Nous, on va plutôt s'intéresser à du digital low resolution.
* On s'intéresse à un système sans _out of band communication_, et dans un modèle LOS/NLOS réaliste.

## II. Design options for mmWave Initial Access

### A. Initial access procedure steps

![](https://i.imgur.com/voQMI8B.png)

Toutes les implémentations proposées suivent le schéma de la figure 1. On se focalisera sur les deux premières étapes uniquement.

#### Etapes de l'IA

0) _Détection du signal de synchronisation_: Chaque cellule envoie périodiquement des signaux pour que les UE puissent détecter la présence de la BS et obtenir le timing des downlink frames. L'UE ne cherche qu'à obtenir la direction à long terme, qui varie peu avec le small scale fading. **On suppose qu'à l'issue de cette étape, l'UE connaît la direction de la BS**.

1) _RA preamble transmission_: Comme en LTE, on suppose qu'il y a des slots dédiés au random access (RA).

2) _Random access response_ (RAR): La BS renvoie une RAR à l'utilisateur concerné. **A cette étape, l'UE et la BS connaissent la BF direction**.

3) _Connection request_: L'UE envoie une requête de connexion (un message Radio Resource Control (RRC) en LTE).

4) _Scheduled communication_: A cette étape, toutes les communications se passent dans les canaux réservés, avec tout le gain directionneL.

### B. Synchronization and random access signals

On présente les notations $T_per$, $T_sig$ et $W_sig$.

### C. Learning the BF directions

On va chercher à spécifier:
1) Comment la BS va envoyer les signaux (directionnels ou pas).
2) Comment l'UE va recevoir ces signaux (de façon directionnelle ou pas).
3) Comment la BS va recevoir les signaux RA (de façon directionnelle ou pas). 

En tout, il y a 5 combinaisons viables à étudier.


### D. Sequential beamspace scanning

![](https://i.imgur.com/gk7bZ1v.png)

Pas mal d'explications, résumées par le tableau II.

## III. Signal detection under a sequential beamspace scanning

### A. Generalized likelihood radio (GLRT) detection:

Ca part dans des délires de signal space representation de dimension M.
En gros, on estime l'erreur de prédiction de la direction d'énergie maximale.

### B Digital and hybrid detection:

Ca revient à qh de proche de l'analog, sinon ça coûte trop d'énergie.

## IV. Delay analysis:

Il y a deux paramètres importants:
1) Le _délai de synchronisation_: Le temps nécessaire à l'UE pour détecter la présence du signal de synchronisation.
2) Le _délai de préambule RA_: Le temps nécessaire à la BS pour détecter le RA preamble à partir du moment où il est envoyé.

_Access delay_: la somme des deux délais ci-dessus.




